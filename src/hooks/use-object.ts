import { useState } from 'react'

export function useObject<T>(initial: T) {
  const [state, setState] = useState(initial)
  function patchState(partial: Partial<typeof state>) {
    setState(state => ({ ...state, ...partial }))
  }
  return { state, patchState }
}
