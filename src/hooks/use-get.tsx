import { IonProgressBar, IonText } from '@ionic/react'
import { useEffect, useState } from 'react'
import useStorageState from 'react-use-storage-state'
import { ErrorMessage } from '../components/ErrorMessage'
import { get } from '../utils/api'

export function useGet<T extends { error: string }>(url: string, initial: T) {
  const [state, setState] = useStorageState(url, initial)
  const [isFirst, setIsFirst] = useStorageState('isFirst:' + url, true)
  const [isLoading, setIsLoading] = useState(false)
  async function download() {
    setIsLoading(true)
    let json = await get(url)
    setTimeout(() => {
      if (!json.error || !json.error.includes('NetworkError') || isFirst) {
        setState(json)
      }
      if (isFirst) {
        setIsFirst(false)
      }
      setIsLoading(false)
    }, 1000)
  }
  useEffect(() => {
    download()
  }, [])
  function render(fn: (state: Omit<T, 'error'>) => any) {
    return (
      <>
        {state.error ? (
          <ErrorMessage error={state.error} />
        ) : (
          <>
            <div
              style={{
                transition: 'opacity 0.15s',
                opacity: isLoading ? 1 : 0,
              }}
            >
              <IonProgressBar type="indeterminate" />
            </div>
            {fn(state)}
          </>
        )}
      </>
    )
  }
  return { state, render, refresh: download }
}
