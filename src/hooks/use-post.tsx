import { useIonToast } from '@ionic/react'
import { useState } from 'react'
import { ErrorMessage } from '../components/ErrorMessage'
import { post } from '../utils/api'

export function usePost<T>(url: string) {
  const [error, setError] = useState('')
  const [present, dismiss] = useIonToast()
  return {
    showToast(message: string) {
      present(message, 3000)
    },
    error,
    renderError() {
      return <ErrorMessage error={error} />
    },
    post: async (body: object, successCallback?: (json: T) => void) => {
      let json = await post(url, body)
      setError(json.error)
      if (!json.error) {
        successCallback?.(json)
      }
    },
  }
}

window.addEventListener('offline', () => {})
window.addEventListener('online', () => {})
