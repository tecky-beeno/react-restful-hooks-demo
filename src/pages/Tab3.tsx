import {
  IonButton,
  IonContent,
  IonHeader,
  IonIcon,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import './Tab3.css'
import * as icons from 'ionicons/icons'
import { useGet } from '../hooks/use-get'
import { usePost } from '../hooks/use-post'

const Tab3: React.FC = () => {
  const report = useGet('/report', { mood: 'refresh', error: '' })

  const reporter = usePost('/report')

  async function submitMood(mood: string) {
    reporter.post({ mood }, json => {
      console.log('submit mood success:', json)
      reporter.showToast('posted successfully')
      report.refresh()
    })
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 3</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        {report.render(state => (
          <IonButton
            onClick={report.refresh}
            style={{
              height: '5rem',
              width: '5rem',
            }}
          >
            <div>
              <IonIcon
                icon={(icons as any)[state.mood]}
                style={{ fontSize: '2rem' }}
              />
              <div>get</div>
            </div>
          </IonButton>
        ))}

        <br />

        {reporter.renderError()}
        <IonButton
          onClick={() => submitMood('pizza')}
          style={{
            height: '5rem',
            width: '5rem',
          }}
        >
          <div>
            <IonIcon icon={icons.pizza} style={{ fontSize: '2rem' }} />
            <div>post</div>
          </div>
        </IonButton>
        <IonButton
          onClick={() => submitMood('happy')}
          style={{
            height: '5rem',
            width: '5rem',
          }}
        >
          <div>
            <IonIcon icon={icons.happy} style={{ fontSize: '2rem' }} />
            <div>post</div>
          </div>
        </IonButton>
      </IonContent>
    </IonPage>
  )
}

export default Tab3
