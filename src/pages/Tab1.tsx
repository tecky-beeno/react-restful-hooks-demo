import {
  IonAvatar,
  IonButton,
  IonButtons,
  IonCard,
  IonContent,
  IonDatetime,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonModal,
  IonPage,
  IonThumbnail,
  IonTitle,
  IonToolbar,
  useIonModal,
} from '@ionic/react'
import { useState } from 'react'
import { useObject } from '../hooks/use-object'
import { DAY, formatDate } from '../utils/format'
import './Tab1.css'

const Tab1: React.FC = () => {
  const minDate = new Date()
  let min = formatDate(minDate)
  let maxDate = new Date(minDate.getTime() + 7 * DAY)
  let max = formatDate(maxDate)

  const { state, patchState } = useObject({
    date: '',
    stylish: '',
    isSelectingStylish: false,
  })
  const stylishList = ['Alice', 'Bob', 'Charlie', 'Dave']

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 1</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonDatetime
          value={state.date}
          onIonChange={e => {
            patchState({ date: e.detail.value || '' })
          }}
          min={min}
          max={max}
        ></IonDatetime>
        sel: {state.date}
        <IonList>
          <IonItem>
            <IonLabel>Stylish: {state.stylish}</IonLabel>
            <IonButton
              slot="end"
              onClick={() => patchState({ isSelectingStylish: true })}
            >
              Choose
            </IonButton>
          </IonItem>
        </IonList>
        <IonModal isOpen={state.isSelectingStylish}>
          <IonHeader>
            <IonToolbar>
              <IonTitle>Choose Stylish</IonTitle>
              <IonButtons slot="end">
                <IonButton
                  onClick={() => patchState({ isSelectingStylish: false })}
                >
                  Dismiss
                </IonButton>
              </IonButtons>
            </IonToolbar>
          </IonHeader>
          <IonContent className="ion-padding">
            {stylishList.map(stylish => (
              <IonCard
                key={stylish}
                onClick={() => {
                  patchState({ stylish, isSelectingStylish: false })
                }}
              >
                <IonAvatar>
                  <img src="/assets/icon/icon.png" />
                </IonAvatar>
                <IonThumbnail>
                  <img src="/assets/shapes.svg" />
                </IonThumbnail>
                <b>{stylish}</b>
              </IonCard>
            ))}
          </IonContent>
        </IonModal>
      </IonContent>
    </IonPage>
  )
}

export default Tab1
