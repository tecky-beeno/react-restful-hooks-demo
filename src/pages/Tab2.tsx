import {
  IonButton,
  IonContent,
  IonHeader,
  IonIcon,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { useEffect, useState } from 'react'
import './Tab2.css'

function formatDuration(duration: number) {
  let ms = duration % 1000
  let remind = (duration - ms) / 1000
  let second = remind % 60
  remind = (remind - second) / 60
  let minute = remind % 60
  remind = (remind - minute) / 60
  let hour = remind
  if (hour < 1) {
    return `${minute} minutes`
  }
  return `${hour} hours ${minute} minutes`
}

const Tab2: React.FC = () => {
  const [checkInTime, setCheckInTime] = useState('')
  const [passedText, setPassedText] = useState('')
  function updatePassedText() {
    const passed = Date.now() - new Date(checkInTime).getTime()
    setPassedText(formatDuration(passed))
  }
  useEffect(() => {
    if (!checkInTime) {
      return
    }
    const timer = setInterval(updatePassedText, 1000 * 5)
    updatePassedText()
    return () => {
      clearInterval(timer)
    }
  }, [checkInTime])
  function checkIn() {
    navigator.geolocation.getCurrentPosition(
      position => {
        console.log('position:', position)
        // position.coords.latitude
        // position.coords.longitude
        // position.coords.accuracy
        setCheckInTime(new Date().toISOString())
      },
      error => {
        console.log('failed to get geo position:', error)
      },
    )
  }
  
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 2</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonButton expand="block" hidden={!!checkInTime} onClick={checkIn}>
          check in
        </IonButton>
        <div hidden={!checkInTime}>
          <div>check in time: {checkInTime}</div>
          <div>passed: {passedText}</div>
        </div>
        <IonButton
          expand="block"
          hidden={!checkInTime}
          onClick={() => setCheckInTime('')}
        >
          check out
        </IonButton>
       
      </IonContent>
    </IonPage>
  )
}

export default Tab2

