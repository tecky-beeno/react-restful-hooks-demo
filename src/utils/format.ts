function d2(num: number) {
  return num < 10 ? '0' + num : num
}

export const SECOND = 1000
export const MINUTE = 60 * SECOND
export const HOUR = 60 * MINUTE
export const DAY = 24 * HOUR

export function formatDate(date: Date) {
  return (
    date.getFullYear() +
    '-' +
    d2(date.getMonth() + 1) +
    '-' +
    d2(date.getDate())
  )
}
