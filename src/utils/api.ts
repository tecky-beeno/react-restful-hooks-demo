export async function handleFetchResult(resP: Promise<Response>) {
  try {
    let res = await resP
    let json = await res.json()
    return json
  } catch (error) {
    return { error: String(error) }
  }
}

let apiOrigin = 'http://localhost:8100'
export function get(url: string) {
  return handleFetchResult(
    fetch(apiOrigin + url, {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    }),
  )
}
export function post(url: string, body?: object) {
  return handleFetchResult(
    fetch(apiOrigin + url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
      body: JSON.stringify(body),
    }),
  )
}
export function upload(url: string, body: FormData) {
  // let body = new FormData()
  // body.append('photo',file)
  return handleFetchResult(
    fetch(apiOrigin + url, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
      body,
    }),
  )
}

export async function getReport() {
  return get('/report')
}

export async function postReport(mood: string) {
  return post('/report', { mood })
}
